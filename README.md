# Basic template for Front-end web development

## Table of Contents

* [General](#general)
* [Gulp](#application)

## General

This template provide a basic structure for Front-end web development projects. Git configuration will ignore node_modules/ and dist/ content by default
There is a windows shorcut to open a console on your project's path (VS08P.bat).

## Gulp

Version installed: 3.9.1. Gulp functions could be upgraded to 4.0 to allow tasks in series and object variable assignment. This will allow cleanest structures on functions. Be sure to have node.js installed (https://nodejs.org/en/download/)

Gulp installation: Go to your project's path on console and run the following commands:

1. npm install gulp-cli -g
2. npm install gulp -D (npm install gulp --latest to install latest version).
3. npm install (to install all dependencies)

default task only will watch over *.html, js/*.js and css/*.css files to reload the page through browserSync. Add your own file/folder if needed.

css-dist/css-dev tasks will require to add each files to be concatenated
js-dist/js-dev tasks will require to add each files to be concatenated

dist task will generate a clean production version of the project on dist/ folder
